desc "This task is called by the Heroku scheduler add-on"

task :send_admin_daily_report => :environment do
  AdminMailer.daily_report.deliver_now
end
