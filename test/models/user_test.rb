# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'normal user should be without providing is_admin' do
    new_user = User.new(
    username: 'unenouvelle', email: 'jrcfdrb@hotmail.com', password: '1232134'
  )
    assert new_user.valid?
  end

  test 'user without email or password should not be valid' do
    new_user = User.new(
    username: 'unenouvelle', is_admin: true, password: '1232134'
  )
    refute new_user.valid?

    new_user = User.new(
    username: 'unenouvelle', is_admin: true, email: 'jrcfdrb@hotmail.com'
  )
    refute new_user.valid?
  end

  test 'user without username should not be valid' do
    new_user = User.new(
    email: 'jrcfdrb@hotmail.com', is_admin: true, password: '1232134'
  )
    refute new_user.valid?
    assert_not_empty new_user.errors[:username]
    assert_empty new_user.errors[:email]
    assert_empty new_user.errors[:password]
    assert_empty new_user.errors[:is_admin]
  end

  test 'username must start with letter and be between 5-30 chars' do
    new_user = User.new(
    username: '33333', email: 'jrcfdrb@hotmail.com', password: '1232134'
  )
    refute new_user.valid?
    assert_not_empty new_user.errors[:username]

    new_user = User.new(
    username: 'x', email: 'jrcfdrb@hotmail.com', password: '1232134'
  )
    refute new_user.valid?
    assert_not_empty new_user.errors[:username]

    new_user = User.new(
    username: 'dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddx',
    email: 'jrcfdrb@hotmail.com', password: '1232134'
  )
    refute new_user.valid?
    assert_not_empty new_user.errors[:username]

    new_user = User.new(
    username: 'Thomasgégé3.0', email: 'jrcfdrb@hotmail.com', password: '1232134'
  )
    assert new_user.valid?
  end
end
