# frozen_string_literal: true

require 'test_helper'
class ListUserTest < ActiveSupport::TestCase
  test 'entry should always have list and user' do
    e = ListUser.new
    refute e.valid?
  end

  test 'user and list N-N, but no duplicate user for list' do
    u1 = users(:user_1)
    u2 = users(:user_2)

    l1 = lists(:list_1)
    l2 = lists(:list_2)

    e1 = ListUser.create(list: l1, user: u1)
    refute e1.valid?
    assert_not_empty e1.errors[:user_id]

    assert_equal u1.lists.count, 4, '1'
    assert_equal u2.lists.count, 0, '2'
    assert_equal l1.users.count, 3, '3'
    assert_equal l2.users.count, 2, '4'
  end
end
