# frozen_string_literal: true

require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  test 'normal task should be valid without providing finished details' do
    t = Task.new(list: lists(:list_1), creator: users(:user_1),
    title: 'harry potter tous les 7')
    assert t.valid?
  end

  test 'list can have multiple tasks' do
    l = lists(:list_2)
    assert_equal l.tasks.count, 4
  end

  test 'title must exist and has a minimum length' do
    t = Task.new(list: lists(:list_1), creator: users(:user_2), title: 'h')
    refute t.valid?
    assert_not_empty t.errors[:title]

    t = Task.new(list: lists(:list_1), creator: users(:user_2))
    refute t.valid?
    assert_not_empty t.errors[:title]
  end

  test 'finished: true and finisher must be present or absent together' do
    t = Task.new(list: lists(:list_1), creator: users(:user_1),
    title: 'harry potter tous les 7', finished: true)
    refute t.valid?, '1'
    assert_not_empty t.errors[:finisher]

    t = Task.new(list: lists(:list_1), creator: users(:user_1),
    title: 'harry potter tous les 7', finisher: users(:user_3))
    refute t.valid?, '2'
    assert_not_empty t.errors[:finisher]

    t = Task.new(list: lists(:list_1), creator: users(:user_1),
    title: 'harry potter tous les 7', finisher: users(:user_3), finished: true)
    assert t.valid?, '3'
    assert_empty t.errors[:finisher]
  end

  test 'creator and finisher must be someone who has access to the list' do
    t = Task.new(list: lists(:list_1), creator: users(:user_1),
    title: 'harry potter tous les 7', finisher: users(:user_5))
    refute t.valid?
    assert_not_empty t.errors[:finisher]
  end
end
