# frozen_string_literal: true

require 'test_helper'

class ListTest < ActiveSupport::TestCase
  test 'a list should be valid without providing any detail in particular' do
    list = List.new
    assert list.valid?
  end

  test 'tag must be within provided list' do
    list = List.new(tag: 'df')
    refute list.valid?
    assert_not_empty list.errors[:tag]
  end

  test "name and description can't be too long" do
    list = List.new(listname: '4444444444444444444444444444444444444444',
    description: 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy
    yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
    refute list.valid?
    assert_not_empty list.errors[:listname]
    assert_not_empty list.errors[:description]
  end
end
