# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:user_1)
    sign_in @user
  end

  test 'should NOT see index if not admin' do
    get users_path
    assert_response :redirect
    assert_equal ApplicationController::ACCESS_ERROR_MESSAGE, flash[:notice]
  end

  test 'should see index if admin' do
    @user = users(:user_5)
    sign_in @user
    get users_path
    assert_response :success
  end

  test 'should see profile' do
    get user_path(@user)
    assert_response :success
  end

  test 'should NOT see profile if NOT loggedin' do
    sign_out(@user)
    get user_path(@user)
    assert_response :redirect
    assert_equal ApplicationController::SIGN_IN_MESSAGE, flash[:notice]
  end

  test 'should NOT have problem if passing a random id' do
    get user_path('hjkndfdhs')
    assert_response :success
  end

  test 'should delete profile' do
    skip
    # this method uses ajax instead of html
    assert_difference('User.count', -1) do
      delete user_path(@user)
    end
    assert_equal 'Votre profile est supprimé. Vous pouvez toujours vous ré-inscrire quand vous aurez besoin.', flash[:notice]
    assert_nil User.find_by(email: @user.email)
  end
end
