### Colab, le gestionnaire de tâche

Click **[here](https://colab-task.herokuapp.com)** to visit the site on Heroku

_Google Fonts are used for this app. It is recommended to open the app in Chrome_

testing accounts:

colab_user0@yopmail.com//1111111
colab_admin@yopmail.com//1111111

* * *
##### Concept and functions

Colab is a web app to manage your to-do lists and share a common list between collaborators. It is a one-page app for normal users, whereas admin has a dedicated page to see user details.

Its original concept is as follows:
* * *

```Sujet : 
J'ai besoin d'une application pour gérer mes taches dans plusieurs domaines.
Il me faut donc une application qui me permet de gérer plusieurs listes de taches.
Je dois pouvoir créer une tache, modifier une tache, cocher une tache une fois que je l'ai réalisé.

Vu que personne n'a jamais créé une application qui permet de gérer des taches, j'aimerai la proposer au public et pouvoir gérer plusieurs utilisateurs qui se connecterai pour gérer leur taches dans des listes.

J'aimerai qu'une fois la tache réalisé elle disparaisse de ma liste. Mais j'aimerai pouvoir la retrouver dans mes archives pour savoir que je l'ai réalisée et quand est-ce que je l'ai réalisée.

Quand je termine toutes les taches d'une liste j'aimerai recevoir un mail qui me félicite. 
Et oui, c'est important d'encourager ces utilisateurs.

Pour la suite j'ai plusieurs idées de feature, dis moi ce que tu en penses:
- J'aurais besoin d'un admin pour voir toutes les listes et toutes les taches de mes utilisateurs.
- J'aimerai qu'on puisse rechercher les taches avec un champs texte. On m'a parlé d'Algolia, je sais pas si ça marche pour ça.
- J'aimerai que certains utilisateurs puissent collaborer sur une liste de taches. Se serait une bonne feature payante ça!
- J'aimerai avoir un reporting tous les matins qui me dise combien j'ai d'utilisateur, combien de taches ont été créées et combien de taches ont été terminées.
```

The above description is translated into the following realised functions,

* Different users can log in the site :heavy_check_mark: 

* Each user can have multiple lists :heavy_check_mark: 

* Each list can contain multiple tasks :heavy_check_mark:

* Each user can create, modify and check "done" their tasks :heavy_check_mark:

* Multiple users can work on the same list :heavy_check_mark:

* When a task is checked "done", it moves to the buttom of the list with a line through, the name of the user finished it and the date the task is finished :heavy_check_mark: (instead of "disappearing from the list" originally asked)

* When the last task of the list is checked "done" or deleted, which means all tasks in the list are finished, all users working on this list recives an email of congrats :heavy_check_mark:

* When the last task of the list is checked "done" or deleted, which means all tasks in the list are finished, the user no longer sees the list in his profile page :heavy_check_mark:
 
* Each user can click on a button to see all lists, including those completely finished :heavy_check_mark:

* Each user can search in history with key words. Lists including one or more tasks containing the key words will be returned as results :heavy_check_mark:

* Site admin has a dedicated page where all  users are listed. Admin can see username and email of any user, can delete their profile or see all their lists and tasks, with indication whether they are finished.:heavy_check_mark:

* Admin recieves (for the sake of easy testing) every 10 min, an email reporting the number of users and tasks, as well as new users, new tasks created and finished in the last 10 mins.:heavy_check_mark:

* * *
##### Time table

The first version of this app is developped in two weeks. Time spent in each part is as following (development happend more or less in the same order)

* BDD diagram, model (written in TDD) : 5.5 hours

* Controllers and 1st ajax : 16h 

* 1st version of front : 6h

* Git + Heroku + CI setup : 6h (one of the great difficulties, had to ask for help from an experienced developper)

* 3 features: archived lists, search bar and admin interface: 9h (3h each)

* 2nd version of front : 8h 

* mailer function for 2 mails : 4h

* Bug check after each Heroku deploiment : 4h

* Final correction: 3h

**Total: 61.5h ( 9 whole days)**

A project Trello is regularly kept and updated. Time spent in such work is not counted.

Due to time limit, and to the developper's great regret, the app is delivered without controller optimization, controller and system tests nor refacto. These shall be done after delivery.

* * *

##### Realisation of functions

Most of the functions are realised in straight-forward logics. Due to time limit, no particular effort are made to massively explore solutions that weren't known before this project, with only two exceptions:

* use of Heroku scheduler and rake job to send the "daily" report

_With a normal admin mailer and a mail view prepared, install the Heroku Scheduler addon. Define a rake job in `scheduler.rake`, set up to launch it every 10 min in heroku._

* use of class method for the search bar

_Create a `search` method in controller, linked to the User#search method, created in the model. The #search method uses `LIKE ?` sql query. Algolia suggested originally was not used, 
mainly due to timing, but also because using and API for such a simple project seemed exagerating._


Other solutions came up include:

* For the User-List N-N relation : a join table

* For double update of task (meaning the normal "update" of a task changing its name and the "check done" update which changes its status): creation of `finish` method

* For deciding whether a list should be archived (thus disappearing from the user's normal profile and triggering the congrats email): creation of an instance method. 
The model provides one List-Task 1-N relation. `archived` method is an instance method in the model `List` that checks whether a list contains at list one task and whether 
the array containing all tasks' `finished` boolean includes `false`. 
In the `lists controller`, after every `finish` and `destroy`, this method is called and a congrats email is sent when necessary.
The logic is twisted, but more really intelligent alternative was found in the tight timing.

* For users to be able to invite other user, use of select list within all existing user, avoiding miss typing the email or inviting unregistered user. 
All users already related to the list are excluded from selection, avoiding duplicates.

* For one page app: use of ajax. A simple "cheating" solution is found : a lot of ajax merely refreshes the page, causing a large number of queries at any action. 
Development was made easy by this way, but an impact on the loading speed is observed.

* For KPIs in the admin daily report: creation of class methods using `where`. If time had allowed, the `scope` option would have been preferred.

* * *

##### Development principles

The developper had the following in mind during the project:

* Push as early and often as possible

* First find a solution. Only when it is possible, find a better one

* 20% of the UI detail means 80% to the users

* Don't get blocked. Ask, move on or abandon.


* * *

##### Difficulties and regrets

CI and testing are two major difficulties in the realisation of this project.

**CI**(external help was involved)

The notion of CI is new to the developper, who had just once successfully configured an app. In the attempt to reproduce the steps for [Colab](https://colab-task.herokuapp.com), 
it turned out localhost wouldn't work, nor would postgres locally. This developper had figured out her first app configuration without guidance nor structured information, 
causing a lot of incoherences in the CI setup of  Colab. After two days of struggle, a senior developper was consulted and corrected the configuration. This is the only external intervention 
in the development. 

The issue was detected at an early stage of development, at the beginning of controllers development. The developper had hoped to work on the app alongside and discover the solution. 
However, by the time the 1st version of app was ready to be test in prod, the issue persisted. The developper inisisted on resolving this issue in order to push to staging, 
so that prod bugs be detected as early as possible.

Ever since correction of this difficulty, pushes to Heroku have been frequent.

**Testing**

The models were developped in TDD. The same method was attempted on the controllers, when the developper realized she didn't know how to test a one-page app. 
Later on, for timing reason, no more tests were added. If time had allowed, the developper would have at least written system tests gladly and look for resources on single-page app testing.

The tests are written in Minitest.

* * *

##### Other limits 

A lot of effort was deliberately put in the front of this project. Due to the timing, back has received less attention than it should. 

* Only strictly the functions asked. Some "logical" functions were not available (user welcoming email. option to change user info by user or admin)

* Controller hardly has control on access (who can access to what, who can modify what etc)

* A lot of actions that may fails have no error messages displayed in UI

* Mails have no css

* Average responsiveness

* Each action triggers an ajax that refreshes almost the whole page. Speed is impaired.

* Things can certainly be refactored


* * *
##### Takeaways

* This developper definitly needs guidance

* Asking for help does limit time loss

* Pushing as early and often as possible proves to be the right thing to do. Almost no prod bugs are encountered in this project 

* Maybe next time. more reflection **before** action. This time, too much back and forth in simple stuff. 
Maybe blocking a time to explore all desired technologies would lead to more opportunites to test new stuff.

* Front can be fun

* * *
##### Technical specifications

* Ruby version: 2.6.4

* Rails version: 6

* gems: devise, faker, table_print, letter_opener, pry, simplecov among others

* Google Fonts are used, Firefox doesn't show the font

