class UserMailer < ApplicationMailer
  default from: 'no-reply@colab.fr'

  def congrats(list)
    @list = list
    users = list.users.map{|u| u.email}
    mail(to: users, subject: 'Colab vous félicite !')
  end
end
