class AdminMailer < ApplicationMailer
  admins = User.where(is_admin:true)
  default :to => admins.all.map(&:email),
        :from => "daily-report@colab.fr"

  def daily_report
    @nb_users = User.all.count
    @nb_tasks = Task.all.count
    @new_users = User.recent.count
    @new_tasks = Task.recent_creation.count
    @new_finish = Task.recent_finish.count
    mail(subject: '[Cobal] Votre (faux) rapport du matin')
  end


end
