# frozen_string_literal: true

# the following manual features are added:
# custom field for devise registration : username
# flash messages for no access case and connect to have access case
# get the current_user to any action conducted in logged environment
# the control of whether a user have access to a certain action
#    before modifying a list or task
class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_action :verify_authenticity_token
  ACCESS_ERROR_MESSAGE =
    'Oups, je ne crois pas que vous avez accès à ce contenu.'
  SIGN_IN_MESSAGE =
    'Envie de profiter de toutes les fonctions cool ? Connectez-vous !'

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) {
      |u| u.permit(:username, :email, :password, :password_confirmation)
    }
  end

  def after_sign_in_path_for(resource)
    user_path(current_user) #your path
  end

  private

  def get_current_user
    if current_user
      @user = current_user
    else
      redirect_to new_user_session_path
      flash[:notice] = ApplicationController::SIGN_IN_MESSAGE
    end
  end

  def test_owner
    unless @list.list_users.include? @user
      redirect_to user_path(@user)
      flash[:notice] = ApplicationController::ACCESS_ERROR_MESSAGE
    end
  end
end
