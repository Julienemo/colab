# frozen_string_literal: true

class ListsController < ApplicationController
  before_action :get_current_user
  before_action :set_list, only: %i[show edit update destroy]
  # before_action :test_owner

  def index
    @lists = @user.lists
  end

  def show; end

  def new
    respond_to do |format|
      @list = List.new
      format.js {}
    end
  end

  def create
    respond_to do |format|
      @list = List.create(list_params)
      ListUser.create(list: @list, user: @user)
      format.js {}
    end
  end

  def edit
    respond_to do |format|
      format.js {}
    end
  end

  def update
    respond_to do |format|
      @list.update(list_params)
      format.js {}
    end
  end

  def destroy
    respond_to do |format|
      @list.destroy
      format.js {}
    end
  end

  private

  def set_list
    @list = List.find(params[:id])
  end

  def list_params
    params.permit(:listname, :tag, :description)
  end
end
