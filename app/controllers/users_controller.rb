# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :get_current_user, except:[:destroy] # see application controller
  before_action :check_admin, only: [:index]

  def index
    @users = User.all
  end

  def show
    keyword = params[:search_field]
    @username = @user.username.capitalize
    @nb_list = @user.lists.count
    @ok = @user.lists.map{|l|  l.archived ? 1 : 0}.sum
    @lists = @user.lists
  end

  def search
    respond_to do |format|
      format.js {}
    end
  end

  def show_archived
    respond_to do |format|
      format.js {}
    end
  end


  def destroy
    User.find(params[:id]).destroy
    respond_to do |format|
      format.js {}
    end
  end

  private

  def check_admin
    unless @user.is_admin
      redirect_to root_path
      flash[:notice] = ApplicationController::ACCESS_ERROR_MESSAGE
    end
  end
end
