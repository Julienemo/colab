# frozen_string_literal: true

class TasksController < ApplicationController
  before_action :get_current_user
  before_action :get_current_list, only: %i[new create edit update finish destroy]
  before_action :set_task, only: %i[edit update destroy]

  def show; end

  def new
    respond_to do |format|
      @task = Task.new
      format.js {}
    end
  end

  def create
    respond_to do |format|
      @task = Task.create(task_params.merge(list: @list, creator: @user))
      format.js {}
    end
  end

  def edit
    respond_to do |format|
      format.js {}
    end
  end

  def update
    respond_to do |format|
      @task.update(task_params)
      format.js {}
    end
  end

  def finish
    @task = Task.find(params[:task_id])
    @task.update(finished: true, finisher: @user)

    respond_to do |format|
      format.js {}
    end
    send_mail_if_list_archived
  end

  def destroy
    respond_to do |format|
      @task.destroy
      format.js {}
    end
    send_mail_if_list_archived
  end

  private

  def send_mail_if_list_archived
    if @task.list.archived
      congrats_send
    end
  end

  def congrats_send
    UserMailer.congrats(@task.list).deliver_now
  end


  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.permit(:title)
  end

  def get_current_list
    @list = List.find(params[:list_id])
  end
end
