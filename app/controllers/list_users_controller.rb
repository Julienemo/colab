# frozen_string_literal: true

class ListUsersController < ApplicationController
  before_action :set_list
  before_action :set_list_user_link, only: [:destroy]

  def new
    @link = ListUser.new
    respond_to do |format|
      format.js {}
    end
  end

  def create
    new_user = User.where(email: user_email)[0]
    respond_to do |format|
      list_user = ListUser.create(user: new_user, list: @list)
      format.js {}
    end
  end

  def destroy
    respond_to do |format|
      @list_user.destroy
      format.js {}
    end
  end

  private

  def user_email
    params.permit(:email)[:email]
  end

  def set_list
    @list = List.find(params[:list_id])
  end

  def set_list_user_link
    @link = ListUser.find(params[:id])
  end
end
