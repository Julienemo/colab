# frozen_string_literal: true

class List < ApplicationRecord
  TAG_LIST = %w[travail école course lecture sport remboursement sortie recette voyage famille].freeze

  validates :tag, inclusion: { in: TAG_LIST, message: 'must be from list' }
  validates :listname, length: { maximum: 30, message: 'must be shorter than 30 chars' }
  validates :description, length: { maximum: 100, message: 'must be shorter than 100 chars' }

  has_many :list_users
  has_many :users, through: :list_users

  has_many :tasks

  def archived
    if tasks.count == 0 || (tasks.map{|t| t.finished}.include?false)
      return false
    else
      return true
    end
  end
end
