# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  START_WITH_LETTER_REGEX = /^\p{L}/.freeze
  RECENT_CRITERION = 10.minutes
end
