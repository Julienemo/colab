# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :list
  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'
  belongs_to :finisher, foreign_key: 'finisher_id', class_name: 'User', optional: true

  validates :title, length: { minimum: 2, message: 'must be longer than 2 chars' }
  validates :title, presence: { message: 'must exist' }
  validate :finished_and_finisher_must_be_present_or_absent_together
  validate :creator_finisher_must_be_among_list_users

  def list_users
    list.users
  end

  def self.recent_creation
    Task.where(created_at: ((Time.now - RECENT_CRITERION)..Time.now))
  end

  def self.recent_finish
    Task.where(created_at: ((Time.now - RECENT_CRITERION)..Time.now)).where(finished: true)
  end

  private

  def finished_and_finisher_must_be_present_or_absent_together
    if finished == true && finisher.nil?
      errors.add(:finisher, 'must exist when task is finished')
    elsif finished == false && !finisher.nil?
      errors.add(:finisher, "mustn't exist when task is unfinished")
    end
  end

  def creator_finisher_must_be_among_list_users
    unless list_users.include? creator
      errors.add(:creator, 'must be among users of this list')
    end
    if !finisher.nil? && !(list_users.include? finisher)
      errors.add(:finisher, 'must be among users of this list')
    end
  end
end
