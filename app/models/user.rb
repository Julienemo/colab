# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :username, presence: { message: 'must exist' }
  validates :username, length: { in: 5..30, message: 'must be between 5-30 chars' }
  validate :user_name_must_start_with_letter

  has_many :list_users
  has_many :lists, through: :list_users
  has_many :tasks_created, foreign_key: 'creator_id', class_name: 'Task'
  has_many :tasks_finished, foreign_key: 'finisher_id', class_name: 'Task'

  def search(n)
    if n
      results = []
      lists.each do |l|
        if l.tasks.where("title LIKE ?", "%#{n}%").count > 0
          results << l
        end
      end
      return results
    else
      return lists
    end
  end

  def self.recent
    User.where(created_at: ((Time.now - RECENT_CRITERION)..Time.now)) # see application record
  end

  private

  def user_name_must_start_with_letter
    # for regex, see Application_record.rb
    if (START_WITH_LETTER_REGEX =~ username).nil?
      errors.add(:username, 'must start with a letter')
    end
  end
end
