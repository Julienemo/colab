# frozen_string_literal: true

class ListUser < ApplicationRecord
  validates :list_id, presence: { message: 'must be present and represent an existing list' }
  validates :user_id, presence: { message: 'must be present and represent an existing user' }
  validates :user_id, uniqueness: { scope: :list_id, message: 'is already attached to this list' }

  belongs_to :user
  belongs_to :list
end
