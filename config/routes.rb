# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    root to: "devise/sessions#new"
  end

  resources :users, only: %i[index show destroy] do
    get :show_archived
    get :search
  end

  resources :lists do
    resources :list_users
    resources :tasks
    resources :tasks do
      put :finish
    end
  end
end
