# frozen_string_literal: true

puts 'Deleting previous record...'
Task.destroy_all
ListUser.destroy_all
List.destroy_all
User.destroy_all

puts 'Generating random data...'
i = 0
5.times do
  User.create(username: Faker::Name.first_name, email: "colab_user#{i}@yopmail.com", password: '1111111')
  i += 1
end
puts "#{User.count} fake users generated."

User.create(username: 'Colab_admin', email: 'colab_admin@yopmail.com', password: '1111111', is_admin: true)
User.create(username: 'Colab_other_admin', email: 'colab_other_admin@yopmail.com', password: '1111111', is_admin: true)

puts '2 fake admins generated.'

i = 1
10.times do
  List.create(tag: List::TAG_LIST.sample, listname: "list nb #{i}", description: Faker::Lorem.sentence(word_count: 2))
  i += 1
end
puts "#{List.count} fake to-do lists generated."

users = User.all.map(&:id)
lists = List.all.map(&:id)
30.times do
  ListUser.create(user_id: users.sample, list_id: lists.sample)
end
puts "#{ListUser.count} user/list matches generated."

active_lists = ListUser.all.map(&:list).uniq
i = 0
active_lists.each do |al|
  4.times do
    t = Task.create(title: "task #{i}", list: al, creator: al.users.sample)
    i += 1
  end
end
puts "#{Task.count} tasks generated. They are all unfinished"
