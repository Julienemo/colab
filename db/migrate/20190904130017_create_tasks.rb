# frozen_string_literal: true

class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.references :list, null: false
      t.references :creator, index: true, null: false
      t.references :finisher, index: true, null: true
      t.string :title, null: false
      t.boolean :finished, default: false
      t.timestamps
    end
  end
end
