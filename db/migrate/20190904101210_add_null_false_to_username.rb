# frozen_string_literal: true

class AddNullFalseToUsername < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :username, :string
    add_column :users, :username, :string, null: false
  end
end
