# frozen_string_literal: true

class ChangeColumnName < ActiveRecord::Migration[6.0]
  def change
    add_column :lists, :listname, :string, default: 'ma liste'
    remove_column :lists, :name, :string, default: 'ma liste'
  end
end
