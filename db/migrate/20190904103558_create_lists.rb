# frozen_string_literal: true

class CreateLists < ActiveRecord::Migration[6.0]
  def change
    create_table :lists do |t|
      t.string :tag, default: 'famille'
      t.string :name, default: 'ma liste'
      t.text :description
      t.string :status, default: 'active'
      t.timestamps
    end
  end
end
